// Anon entry point for whatever UI library you're using, makes sure that coupling is kept to a minimum.
var gameUI = (function(){

	/// WIP - will split all this out later.
	// App
	var app = angular.module('gameui', []),
	localController; // Exposed so we can set data on it, doesn't leave this context however.

	// Our volatile game data to observe. The controller should set this data really.
	var GameDataService = function()
	{

		this.gameData = {
			playerName: "Not set",
			currentWeapon: "",
			score: 0,
			power: 0,
			specials: 0,
			shields: 0
		}

	};

	GameDataService.prototype.get = function(str)
	{

		return this.gameData[str];

	};

	GameDataService.prototype.set = function(str, data) {

		if(typeof data === "object")
		{
			console.error("Sorry, no complex types allowed.");
			return;
		}

		var currentData = this.gameData[str];

		if(typeof currentData !== "undefined") {

			if (typeof currentData !== typeof data) {
				console.error("Data values not of same type, got: " + typeof currentData + " : " + typeof data);
				return;
			}

			// We do it this way as if we bind to a var, it becomes immutable.
			this.gameData[str] = data;

		} else {
			console.error("No data found of name '" + str + "'.");
		}

	};

	GameDataService.prototype.populate = function(obj) {

		var currentData;

		angular.forEach(obj, function(value, key){

			currentData = this.gameData[key];

			if(typeof currentData !== "undefined") {
				this.set(key, value);
			} else {
				console.error("Property " + key + " is not allowed.");
			}

		}, this);

	};

	app.service('GameDataService', GameDataService);

	// Controllers
	var UIController = function($scope, dataService){

		this.dataService = dataService;
		this.scope = $scope;
		// So the app can access methods
		localController = this;

	};

	UIController.prototype.get = function(str)
	{

		return this.dataService.get(str);

	};

	UIController.prototype.set = function(str, data)
	{

		this.dataService.set(str, data);
		this.refreshData();

	};

	UIController.prototype.populate = function(obj)
	{

		this.dataService.populate(obj);
		this.refreshData();

	};

	UIController.prototype.refreshData = function()
	{

		// Apparently the only way I could find to make it work.
		var self = this;
		this.scope.$apply(function() {
		    self.playerName = self.dataService.get("playerName");
		    self.score = self.dataService.get("score");
			self.power = self.dataService.get("power");
			self.currentWeapon = self.dataService.get("currentWeapon");
			self.specials = self.dataService.get("specials");
			self.shields = self.dataService.get("shields");
			// There's probably a nice way to do these also, perhaps make use of ng-model instead.
			self.scope.powerBarWidth = self.power > 0 ? self.power + "%" : self.power;
			self.scope.overdrive = self.shields > 100;
			self.scope.nearDeath = self.shields < 31;
		});

	};

	UIController.$inject = ['$scope', 'GameDataService'];
	app.controller('UIControl', UIController);

	// Manually load the app now UI shell is ready.
	angular.element(document).ready(function() {
      angular.bootstrap(document, ['gameui']);
    });

	// Exposes these publically and nothing else.
	return {
		populate: function(obj) {

			localController.populate(obj);
		
		},
		set: function(str, data) {
			
			localController.set(str, data);

		},
		get: function(str) {
			
			return localController.get(str); 

		}
	}

})();