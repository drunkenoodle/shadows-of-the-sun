define(['game'], function(game){

	/// Basic square path ///
	var SquarePath = function(){

		this.mode = 1;
		this.pathIndex = 0;
		this.path = [];

        // Should give the impression of speed
        this.pathStepping = 1;

        // Variable - Will be set from within data
		this.points = {
	        'x': [ game.world.width - 132, game.world.width - 264, game.world.width - 264, game.world.width - 132, game.world.width - 132 ],
	        'y': [ 32, 32, 164, 164, 32 ]
	    };

	    // To generate randomly
        // var py = this.points.y;

        // for (var i = 0; i < py.length; i++)
        // {
        //     py[i] = this.rnd.between(40, game.world.height - 40);
        // }

        this.plot();

        return this;

	};

    SquarePath.prototype.resetPathIndex = function() {
        this.pathIndex = 0;
    };

	SquarePath.prototype.plot = function () {

        this.path = [];

        var x = 1 / game.width;

        for (var i = 0; i <= 1; i += x)
        {
            if (this.mode === 0)
            {
                var px = game.math.linearInterpolation(this.points.x, i);
                var py = game.math.linearInterpolation(this.points.y, i);
            }
            else if (this.mode === 1)
            {
                var px = game.math.bezierInterpolation(this.points.x, i);
                var py = game.math.bezierInterpolation(this.points.y, i);
            }
            else if (this.mode === 2)
            {
                var px = game.math.catmullRomInterpolation(this.points.x, i);
                var py = game.math.catmullRomInterpolation(this.points.y, i);
            }

            this.path.push( { x: px, y: py });
        }

    };

    SquarePath.prototype.changeMode = function () {

        this.mode++;

        if (this.mode === 3)
        {
            this.mode = 0;
        }

        this.plot();

    };

    SquarePath.prototype.setPathStepping = function(n) {
        
        this.pathStepping = n;

    };

    SquarePath.prototype.updatePaths = function() {

    	// Cheap and cheerful (may use physics instead, and set from parent)
        this.pathIndex += this.pathStepping;

        // Just as a test, changes modes one each ending of path reached.
        if (this.pathIndex >= this.path.length)
        {
            this.pathIndex = 0;
            this.changeMode();
        }

    };

    SquarePath.prototype.getNextNode = function() {
    	
    	return this.path[this.pathIndex];

    };

	return SquarePath;

});