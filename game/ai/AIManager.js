define(["Phaser", "PathRegistry"], function(Phaser, PathRegistry){
	
	var AIManager = function () {

        this.parent = null;
        this.currentPath = null;
        return this;

	}, nextNode;

    AIManager.prototype.bindToThis = function(context, options) {

        this.parent = context;

        //// To tidy up
        // Path-based AI
        if(options.pathType)
            this.currentPath = new PathRegistry.paths[options.pathType];

        // Little bit of a hack to make it look like path speed increases. Really we need to directly
        // make a physics equasion on the actual enemy to be honest. As currently it's just skipping path
        // steps which will look ugly if going to fast.
        if(this.currentPath && options.speedAlongPath)
            this.currentPath.setPathStepping(options.speedAlongPath);

        // Static-based AI
        // ...

    }

    AIManager.prototype.update = function() {

        if(this.currentPath && this.currentPath.path.length) {

            // Need to call this to step through each path node.
            this.currentPath.updatePaths();

            // pathtest - http://phaser.io/tutorials/coding-tips-008
            nextNode = this.currentPath.getNextNode();
            this.parent.staticMove(nextNode.x, nextNode.y);

        }

    }

    AIManager.prototype.reset = function() {

        if(this.currentPath) {
            this.currentPath.resetPathIndex();
            this.currentPath.plot();
        }
        
    }

    return AIManager;

});