define([
	"ai/paths/TestPath",
	"ai/paths/TestPathReversed",
	"ai/paths/RightToLeft"
], function(TestPath, TestPathReversed, RightToLeft){

	return {
		paths: {
			TestPath: TestPath,
			TestPathReversed: TestPathReversed,
			RightToLeft: RightToLeft
		}
	}

});