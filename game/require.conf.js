var require = {
    paths: {
    	"gameUI": "../ui/ui",
    	"Phaser": "../libs/phaser.min",
        "helpers": "helpers/helpers",
        "WeaponRegistry": "weapons/WeaponRegistry",
        "BaseWeapon": "weapons/BaseWeapon",
     	"AIManager": "ai/AIManager",
     	"PathRegistry": "ai/PathRegistry",
        "Bullet": "sprites/Bullet",
        "BaseEnemy": "sprites/BaseEnemy",
        "creatureFactory": "helpers/creatureFactory",
        "pickupFactory": "helpers/pickupFactory",
        "mapFactory": "helpers/mapFactory"
    },
    shim: {
    	"gameUI": {
    		"exports": "gameUI"
    	}
    }
};