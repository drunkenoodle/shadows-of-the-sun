define(["Phaser"], function(Phaser){
	
	var Pickup = function (game, x, y, key) {

		Phaser.Sprite.call(this, game, x, y, key);

        // Not entirly sure what this does
        this.texture.baseTexture.scaleMode = PIXI.scaleModes.NEAREST;

        this.anchor.set(0.5);

        this.checkWorldBounds = true;
        // this.outOfBoundsKill = true; // Should only be true if you've entered the stage first off.
        this.exists = true;
        game.add.existing(this);

        // Variable
        this.value = 30;
        this.type = -1;
        this.elementType = -1;
        this.initialBindingIndex = -1;

	};

	Pickup.prototype = Object.create(Phaser.Sprite.prototype);
    Pickup.prototype.constructor = Pickup;

    Pickup.prototype.doDeath = function(destroy)
    {

        // Might be too slow to create.
        var explosion = this.game.add.sprite(this.body.x, this.body.y, "explosion");

        explosion.anchor.setTo(0.5,0.5);
        explosion.animations.add('explode');
        explosion.animations.play('explode', 60, false, true);

        // May cause issues with above, test thoroughly.
        if(destroy) {
            this.destroy();
        } else {
            this.kill();
        }

    };

    Pickup.prototype.respawn = function(x, y)
    {

        // Reset all positions, scales and stats.
        this.reset(x, y);

        // Not quite right, ensure it's done properly using damage call, etc.
        // http://phaser.io/docs/2.4.4/Phaser.Sprite.html
        // http://phaser.io/docs/2.4.4/Phaser.Group.html#getFirstDead
        this.alive = true;

    };

    return Pickup;

});