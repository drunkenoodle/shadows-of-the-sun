 define(["Phaser", "BaseEnemy"], function(Phaser, BaseEnemy){
	
	var TurretAlpha = function (game, x, y, key, entityProperties) {

		BaseEnemy.call(this, game, x, y, key, entityProperties);

	}

	TurretAlpha.prototype = Object.create(BaseEnemy.prototype);
    TurretAlpha.prototype.constructor = TurretAlpha;

    TurretAlpha.prototype.update = function()
    {
    	
        // Always call this if you want AI updates
        BaseEnemy.prototype.update.call(this);

        if(this.flashing)
            this.checkFlash();

    }

    return TurretAlpha;

});