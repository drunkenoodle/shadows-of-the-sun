 define(["Phaser", "BaseEnemy"], function(Phaser, BaseEnemy){
	
	var TestEnemyWithPath = function (game, x, y, key, entityProperties) {

		BaseEnemy.call(this, game, x, y, key, entityProperties);

        this.ethruster = game.add.sprite(16, 0, 'thrustalpha');
        this.ethruster.animations.add('default');
        this.ethruster.animations.play('default', 24, true);
        this.ethruster.anchor.set(0.5);
        this.ethruster.scale.x *= -1;
        this.addChild(this.ethruster);

	}

	TestEnemyWithPath.prototype = Object.create(BaseEnemy.prototype);
    TestEnemyWithPath.prototype.constructor = TestEnemyWithPath;

    TestEnemyWithPath.prototype.update = function()
    {
    	
        // Always call this if you want AI updates
        BaseEnemy.prototype.update.call(this);

        if(this.flashing)
            this.checkFlash();

    }

    return TestEnemyWithPath;

});