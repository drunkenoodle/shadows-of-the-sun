 define(["Phaser", "BaseEnemy"], function(Phaser, BaseEnemy){
	
	var Cluster = function (game, x, y, key, entityProperties) {

        x = game.width / 2;
        y = game.world.height / 2;

		BaseEnemy.call(this, game, x, y, key, entityProperties);

        this.cursors = game.input.keyboard.createCursorKeys();

        this.sections = [];
        this.pointPath = [];
        this.sectionAmount = 10;
        this.sectionSpacer = 16;

        this.ethruster = game.add.sprite(16, 0, 'thrustalpha');
        this.ethruster.animations.add('default');
        this.ethruster.animations.play('default', 24, true);
        this.ethruster.anchor.set(0.5);
        this.ethruster.scale.x *= -1;
        this.addChild(this.ethruster);

        //  Init snakeSection array (we can make this better obviously, right now we can't hit its children)
        for (var i = 1; i <= this.sectionAmount - 1; i++)
        {
            this.sections[i] = game.add.sprite(x, y, key);
            this.sections[i].anchor.setTo(0.5, 0.5);

            this.ethruster = game.add.sprite(16, 0, 'thrustalpha');
            this.ethruster.animations.add('default');
            this.ethruster.animations.play('default', 24, true);
            this.ethruster.anchor.set(0.5);
            this.ethruster.scale.x *= -1;
            this.sections[i].addChild(this.ethruster);
            
        }

        // Amount of followers
        for (var i = 0; i <= this.sectionAmount * this.sectionSpacer; i++)
        {
            this.pointPath[i] = new Phaser.Point(x, y);
        }

	}

	Cluster.prototype = Object.create(BaseEnemy.prototype);
    Cluster.prototype.constructor = Cluster;

    Cluster.prototype.update = function()
    {
    	
        // Always call this if you want AI updates
        BaseEnemy.prototype.update.call(this);

        this.body.velocity.setTo(0, 0);
        this.body.angularVelocity = 0;

        // Do an if moving check to ensure you're not doing this calculation each time.
        this.body.velocity.copyFrom(this.game.physics.arcade.velocityFromAngle(this.angle, 300));

        // Everytime the snake head moves, insert the new location at the start of the array, 
        // and knock the last position off the end
        var part = this.pointPath.pop();

        part.setTo(this.x, this.y);

        this.pointPath.unshift(part);

        for (var i = 1; i <= this.sectionAmount - 1; i++)
        {
            this.sections[i].x = (this.pointPath[i * this.sectionSpacer]).x;
            this.sections[i].y = (this.pointPath[i * this.sectionSpacer]).y;
        }

    }

    return Cluster;

});