define(["Phaser"], function(Phaser){
	
    //  Our core Bullet class
    //  This is a simple Sprite object that we set a few properties on
    //  It is fired by all of the Weapon classes
	var Bullet = function (game, key) {

		Phaser.Sprite.call(this, game, 0, 0, key);

        this.texture.baseTexture.scaleMode = PIXI.scaleModes.NEAREST;

        this.anchor.set(0.5);

        this.checkWorldBounds = true;
        this.outOfBoundsKill = true;
        this.exists = false;
        this.dontKillOnHit = false;

        this.tracking = false;
        this.scaleSpeed = 0;

        // Should be fixed for each bullet type and only set once. TODO: Set from parent instead.
        this.baseDamage = 10;
        this.maxDamage = 100;

        // Should be variable depending on environment (pickups, etc)
        // This so far can keep going until max is hit above. This could vary.
        this.extraDamage = 0;

	}

	Bullet.prototype = Object.create(Phaser.Sprite.prototype);
    Bullet.prototype.constructor = Bullet;

    Bullet.prototype.fire = function (x, y, angle, speed, gx, gy) {

        gx = gx || 0;
        gy = gy || 0;

        this.reset(x, y);
        this.scale.set(1);

        this.game.physics.arcade.velocityFromAngle(angle, speed, this.body.velocity);

        this.angle = angle;

        this.body.gravity.set(gx, gy);

    };

    Bullet.prototype.update = function () {

        if (this.tracking)
        {
            this.rotation = Math.atan2(this.body.velocity.y, this.body.velocity.x);
        }

        if (this.scaleSpeed > 0)
        {
            this.scale.x += this.scaleSpeed;
            this.scale.y += this.scaleSpeed;
        }

        // On weapon upgrades:
        // Tell you what, rather than just scale the bullet, just change the weapon
        // to another one. You can easily make upgrade steps for them then! In fact,
        // if you do that, you might not even need the weapon power indicator at all.
        // So, a new weapon for each upgrade step. 4 levels should do the trick.

    };

    Bullet.prototype.doDeath = function()
    {

        // Wont stop at enemy hit, good for when you want to damage a line of sprites.
        if(this.dontKillOnHit)
            return;

        // Might be too slow to create.
        var explosion = this.game.add.sprite(this.body.x, this.body.y, "explosion");

        explosion.anchor.setTo(0.5,0.5);
        explosion.animations.add('explode');
        explosion.animations.play('explode', 60, false, true);

        // May cause issues with above, test thoroughly.
        this.kill(); // Allows us to get first existing rather than re-create (as with bullets)

    };

    Bullet.prototype.reduceDamageBy = function(value)
    {

        if(this.extraDamage - value <= 0) {
            this.extraDamage = 0;
            return;
        }

        if(value < 0)
        {
            console.error("Value must be zero or above.");
            return;
        }

        this.extraDamage -= value;

    };

    Bullet.prototype.increaseDamageBy = function(value)
    {

        if(this.extraDamage + value > this.maxDamage) {
            value = this.maxDamage;
            return;
        }

        if(value < 0)
        {
            console.error("Value must be zero or above.");
            return;
        }

        this.extraDamage += value;

    };

    Bullet.prototype.resetDamage = function()
    {
        this.extraDamage = 0;
    }

    Bullet.prototype.getDamage = function()
    {
        return this.baseDamage + this.extraDamage;
    }

    return Bullet;

});