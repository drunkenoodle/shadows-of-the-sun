define(["Phaser", "BaseEnemy"], function(Phaser, BaseEnemy){
	
	var TestEnemy = function (game, x, y, key, entityProperties) {

		BaseEnemy.call(this, game, x, y, key, entityProperties);

        this.ethruster = game.add.sprite(16, 0, 'thrustalpha');
        this.ethruster.animations.add('default');
        this.ethruster.animations.play('default', 24, true);
        this.ethruster.anchor.set(0.5);
        this.ethruster.scale.x *= -1;
        this.addChild(this.ethruster);

	}

	TestEnemy.prototype = Object.create(BaseEnemy.prototype);
    TestEnemy.prototype.constructor = TestEnemy;

    TestEnemy.prototype.update = function()
    {
    	
        // Always call this if you want AI updates
        BaseEnemy.prototype.update.call(this);

        if(this.flashing)
            this.checkFlash();

    }

    return TestEnemy;

});