define(["Phaser", "AIManager"], function(Phaser, AI){
	
	var BaseEnemy = function (game, x, y, key, entityProperties) {

		Phaser.Sprite.call(this, game, x, y, key);

        this.entityProperties = entityProperties;

        // Not entirly sure what this does
        this.texture.baseTexture.scaleMode = PIXI.scaleModes.NEAREST;

        this.anchor.set(0.5);

        this.checkWorldBounds = true;
        // this.outOfBoundsKill = true; // Should only be true if you've entered the stage first off.
        this.exists = true;
        game.add.existing(this);

        // Or this
        this.tracking = false;
        this.scaleSpeed = 0;

        // Flash on hit
        this.nextFlash = 0;
        this.flashRate = 20;
        this.flashing = false;

        // Variable
        this.setHealth();

        // Set up a mind template for AI to attach to
        this.loadMind();

	};

	BaseEnemy.prototype = Object.create(Phaser.Sprite.prototype);
    BaseEnemy.prototype.constructor = BaseEnemy;

    BaseEnemy.prototype.update = function()
    {

        // Experimental, and is moving entity from within (should move out to here)
        this._mind.update();

    }

    BaseEnemy.prototype.staticMove = function(x, y) {

        // Beware this may not call at the right time in update cycle.
        this.x = x;
        this.y = y;

    }

    BaseEnemy.prototype.loadMind = function(){

        this._mind = new AI(); // Returns as constructor

        if(!this.entityProperties) {
            console.warn("Awoke entity with no properties.");
            return this;
        }

        this._mind.bindToThis(this, this.entityProperties);

        return this;

    };

    BaseEnemy.prototype.takeDamage = function(amount) {

        this.health -= amount;

        // If not overriden, just takes damage without factoring anything in.
        if(this.health < 1) {
            this.health = 0;
            this.doDeath();
        }

        if(!this.flashing)
            this.startFlash();

    };

    BaseEnemy.prototype.doDeath = function()
    {

        // Might be too slow to create.
        var explosion = this.game.add.sprite(this.body.x, this.body.y, "explosion");

        explosion.anchor.setTo(0.5,0.5);
        explosion.animations.add('explode');
        explosion.animations.play('explode', 60, false, true);

        // May cause issues with above, test thoroughly.
        this.kill(); // Allows us to get first existing rather than re-create (as with bullets)
        // this.destroy();

    };

    BaseEnemy.prototype.respawn = function(x, y)
    {

        //// Reset all positions, scales and stats.
        // Reset I think resets absolutely everything to a point before any properties are set manually by us, health included.
        this.reset(0, 0);

        this.scale.set(1);
        this.setHealth();
        this._mind.reset();

        // Not quite right, ensure it's done properly using damage call, etc.
        // http://phaser.io/docs/2.4.4/Phaser.Sprite.html
        // http://phaser.io/docs/2.4.4/Phaser.Group.html#getFirstDead
        this.alive = true;

    };

    BaseEnemy.prototype.setHealth = function()
    {
        
        // Just for testings.
        this.health = 140;

    };

    // For this to work you'll have to stick it in the child instance as we don't want it on by default.
    BaseEnemy.prototype.startFlash = function ()
    {
        this.nextFlash = this.game.time.time + this.flashRate;
        this.flashing = true;
    };

    BaseEnemy.prototype.checkFlash = function () {

        this.tint = 0x000000;

        if (this.game.time.time < this.nextFlash) {
            return;
        }

        this.tint = 0xffffff;
        this.flashing = false;

    };

    return BaseEnemy;

});