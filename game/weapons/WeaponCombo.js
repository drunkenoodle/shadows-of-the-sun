define(["Bullet", "Phaser", "game"], function(Bullet, Phaser, game){

    /////////////////////////////////////////////
    //  A Weapon Combo - Automatic (make dyna) //
    /////////////////////////////////////////////

    var Combo = function () {

        return this;

    };

    Combo.prototype.getName = function() {

        return this.name;

    }

    Combo.prototype.combine = function(str, wA, wB, wC) {

    	this.name = str || "Un-named Combination";

    	if(wA)
	    	this.weapon1 = new wA(game);
    
    	if(wB)
	    	this.weapon2 = new wB(game);

    	if(wC)
	    	this.weapon3 = new wC(game);

        return this;

    }

    Combo.prototype.addPaddingX = function(val) {

        if(this.weapon1)
            this.weapon1.x += val;

        if(this.weapon2)
            this.weapon2.x += val;

        if(this.weapon3)
            this.weapon3.x += val;

    }

    Combo.prototype.reset = function () {

    	if(this.weapon1) {
	        this.weapon1.visible = false;
	        this.weapon1.callAll('reset', null, 0, 0);
	        this.weapon1.setAll('exists', false);
        }

        if(this.weapon2) {
	        this.weapon2.visible = false;
	        this.weapon2.callAll('reset', null, 0, 0);
	        this.weapon2.setAll('exists', false);
        }

        if(this.weapon3) {
	        this.weapon3.visible = false;
	        this.weapon3.callAll('reset', null, 0, 0);
	        this.weapon3.setAll('exists', false);
        }

    };

    Combo.prototype.fire = function (source) {

    	if(this.weapon1)
	        this.weapon1.fire(source);
    
        if(this.weapon2)
	        this.weapon2.fire(source);

	    if(this.weapon3)
	        this.weapon3.fire(source);

    };

    return Combo;

})