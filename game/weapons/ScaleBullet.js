define(["Bullet", "BaseWeapon", "Phaser"], function(Bullet, BaseWeapon, Phaser){

    ////////////////////////////////////////////////////////////////////////
    //  A single bullet that scales in size as it moves across the screen //
    ////////////////////////////////////////////////////////////////////////

    var ScaleBullet = function (game) {

        BaseWeapon.call(this, game, game.world, 'Scale Bullet', false, true, Phaser.Physics.ARCADE);

        this.nextFire = 0;
        this.bulletSpeed = 800;
        this.fireRate = 100;

        for (var i = 0; i < 32; i++)
        {
            this.add(new Bullet(game, 'bullet9'), true);
        }

        this.setAll('scaleSpeed', 0.05);

        return this;

    };

    ScaleBullet.prototype = Object.create(BaseWeapon.prototype);
    ScaleBullet.prototype.constructor = ScaleBullet;


    ScaleBullet.prototype.fire = function (source) {

        if (this.game.time.time < this.nextFire) { return; }

        var x = source.x + 10;
        var y = source.y + 10;

        this.getFirstExists(false).fire(x, y, 0, this.bulletSpeed, 0, 0);

        this.nextFire = this.game.time.time + this.fireRate;

    };

    return ScaleBullet;

})