define(["Bullet", "BaseWeapon", "Phaser"], function(Bullet, BaseWeapon, Phaser){

    //////////////////////////////////////////////////////
    //  3-way Fire (directly above, below and in front) //
    //////////////////////////////////////////////////////

    var ThreeWay = function (game) {

        BaseWeapon.call(this, game, game.world, 'Three Way', false, true, Phaser.Physics.ARCADE);

        this.nextFire = 0;
        this.bulletSpeed = 600;
        this.fireRate = 100;

        for (var i = 0; i < 96; i++)
        {
            this.add(new Bullet(game, 'bullet7'), true);
        }

        return this;

    };

    ThreeWay.prototype = Object.create(BaseWeapon.prototype);
    ThreeWay.prototype.constructor = ThreeWay;

    ThreeWay.prototype.fire = function (source) {

        if (this.game.time.time < this.nextFire) { return; }

        var x = source.x + 10;
        var y = source.y + 10;

        this.getFirstExists(false).fire(x, y, 270, this.bulletSpeed, 0, 0);
        this.getFirstExists(false).fire(x, y, 0, this.bulletSpeed, 0, 0);
        this.getFirstExists(false).fire(x, y, 90, this.bulletSpeed, 0, 0);

        this.nextFire = this.game.time.time + this.fireRate;

    };

    return ThreeWay;

})