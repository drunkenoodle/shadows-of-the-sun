define([
	"weapons/SingleBullet",
	"weapons/Beam",
	"weapons/EightWay",
	"weapons/FrontAndBack",
	"weapons/Pattern",
	"weapons/Rockets",
	"weapons/ScaleBullet",
	"weapons/ScatterShot",
	"weapons/SplitShot",
	"weapons/ThreeWay",
	"weapons/WeaponCombo"
], function(SingleBullet, Beam, EightWay, FrontAndBack, Pattern, Rockets, ScaleBullet, ScatterShot, SplitShot, ThreeWay, WeaponCombo){

	return {
		SingleBullet: SingleBullet,
		Beam: Beam,
		EightWay: EightWay,
		FrontAndBack: FrontAndBack,
		Pattern: Pattern,
		Rockets: Rockets,
		ScaleBullet: ScaleBullet,
		ScatterShot: ScatterShot,
		SplitShot: SplitShot,
		ThreeWay: ThreeWay,
		WeaponCombo: WeaponCombo
	}

});