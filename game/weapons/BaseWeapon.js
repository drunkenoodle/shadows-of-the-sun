define(["Phaser"], function(Phaser){
	
	var BaseWeapon = function (game, parent, name, addToStage, enableBody, physicsBodyType) {

        Phaser.Group.call(this, game, parent, name, addToStage, enableBody, physicsBodyType);

        this.type = -1;
        this.upgradeType = -1;
        this.elementType = -1;

        return this;

	};

	BaseWeapon.prototype = Object.create(Phaser.Group.prototype);
    BaseWeapon.prototype.constructor = BaseWeapon;

    BaseWeapon.prototype.setType = function(obj)
    {

        this.type = obj.type;

        if(!isNaN(obj.elementType))
            this.elementType = obj.elementType;

        return this;

    }

    BaseWeapon.prototype.setUpgrade = function(upgradeType)
    {

        this.upgradeType = upgradeType.type;

        return this;

    }

    BaseWeapon.prototype.increaseBulletDamage = function(amount)
    {

        this.children.forEach(function(child){

            child.increaseDamageBy( amount );

        });

    };

    BaseWeapon.prototype.decreaseBulletDamage = function(amount)
    {

        this.children.forEach(function(child){

            child.decreaseDamageBy( amount );

        });

    };

    BaseWeapon.prototype.resetBulletDamage = function(amount)
    {

        this.children.forEach(function(child){

            child.resetDamage();

        });

    };

    BaseWeapon.prototype.getName = function() {

        return this.name;

    };

    return BaseWeapon;

});