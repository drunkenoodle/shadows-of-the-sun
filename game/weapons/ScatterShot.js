define(["Bullet", "BaseWeapon", "Phaser"], function(Bullet, BaseWeapon, Phaser){

    ////////////////////////////////////////////////////
    //  Bullets are fired out scattered on the y axis //
    ////////////////////////////////////////////////////

    ScatterShot = function (game) {

        BaseWeapon.call(this, game, game.world, 'Scatter Shot', false, true, Phaser.Physics.ARCADE);

        this.nextFire = 0;
        this.bulletSpeed = 600;
        this.fireRate = 40;

        for (var i = 0; i < 32; i++)
        {
            this.add(new Bullet(game, 'bullet5'), true);
        }

        return this;

    };

    ScatterShot.prototype = Object.create(BaseWeapon.prototype);
    ScatterShot.prototype.constructor = ScatterShot;

    ScatterShot.prototype.fire = function (source) {

        if (this.game.time.time < this.nextFire) { return; }

        var x = source.x + 16;
        var y = (source.y + source.height / 2) + this.game.rnd.between(-10, 10);

        this.getFirstExists(false).fire(x, y, 0, this.bulletSpeed, 0, 0);

        this.nextFire = this.game.time.time + this.fireRate;

    };

    return ScatterShot;

})