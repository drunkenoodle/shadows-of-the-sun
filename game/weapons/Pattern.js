define(["Bullet", "BaseWeapon", "Phaser"], function(Bullet, BaseWeapon, Phaser){

    ///////////////////////////////////////////////////////////////////////
    //  Bullets have Gravity.y set on a repeating pre-calculated pattern //
    ///////////////////////////////////////////////////////////////////////

    var Pattern = function (game) {

        BaseWeapon.call(this, game, game.world, 'Pattern', false, true, Phaser.Physics.ARCADE);

        this.nextFire = 0;
        this.bulletSpeed = 600;
        this.fireRate = 40;

        this.pattern = Phaser.ArrayUtils.numberArrayStep(-800, 800, 200);
        this.pattern = this.pattern.concat(Phaser.ArrayUtils.numberArrayStep(800, -800, -200));

        this.patternIndex = 0;

        for (var i = 0; i < 64; i++)
        {
            this.add(new Bullet(game, 'bullet4'), true);
        }

        return this;

    };

    Pattern.prototype = Object.create(BaseWeapon.prototype);
    Pattern.prototype.constructor = Pattern;

    Pattern.prototype.fire = function (source) {

        if (this.game.time.time < this.nextFire) { return; }

        var x = source.x + 20;
        var y = source.y + 10;

        this.getFirstExists(false).fire(x, y, 0, this.bulletSpeed, 0, this.pattern[this.patternIndex]);

        this.patternIndex++;

        if (this.patternIndex === this.pattern.length)
        {
            this.patternIndex = 0;
        }

        this.nextFire = this.game.time.time + this.fireRate;

    };

    return Pattern;

})