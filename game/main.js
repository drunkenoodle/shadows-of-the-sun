// Too many requires will get messy, we can trim this down later on
require([
    "helpers",
    "game",
    "gameUI",
    "WeaponRegistry",
    "creatureFactory",
    "pickupFactory",
    "mapFactory" // Bug: Game isn't always ready, either pass to creation or make it wait.
], function(helpers, game, gameUI, WeaponRegistry, creatureFactory, pickupFactory, mapFactory){

    var godmode = true;

    // to move (needs to scale with ui)
    var pixel = { scale: 3, canvas: null, context: null, width: 0, height: 0 };

    // Remember, element type is NOT the same as type.
    var pickups = {
        POWER: {
            type: 0,
            value: 30,
            elementTypes: {
                gatling: 0,
                beam: 1
            }
        },
        SHIELDS: {
            type: 1,
            value: 30
        },
        SHIELDSOV: {
            type: 2,
            value: 255
        },
        SPECIAL: {
            type: 3,
            value: 1
        },
        SHIELD_UPGRADE: {
            type: 4,
            value: 20
        },
        SPECIAL_UPGRADE: {
            type: 5,
            value: 1
        }
    };

    // Must match the array they're in
    var weapons = {
        SINGLEBULLET: {
            type: 0,
            elementType: pickups.POWER.elementTypes.gatling
        },
        FRONTANDBACK: {
            type: 1,
            elementType: pickups.POWER.elementTypes.gatling
        },
        BEAM: {
            type: 2
        }
    }

    var PhaserGame = function () {

        this.background = null;
        this.foreground = null;

        this.player = null;
        this.cursors = null;
        this.speed = 100;

        this.weaponsCache = [];
        this.currentWeapon = weapons.SINGLEBULLET.type;

    };    

    PhaserGame.prototype = {

        init: function () {

            //  Hide the un-scaled game canvas
            game.canvas.style['display'] = 'none';
         
            //  Create our scaled canvas. It will be the size of the game * whatever scale value you've set
            pixel.canvas = Phaser.Canvas.create(game.width * pixel.scale, game.height * pixel.scale);
         
            //  Store a reference to the Canvas Context
            pixel.context = pixel.canvas.getContext('2d');
         
            //  Add the scaled canvas to the DOM
            Phaser.Canvas.addToDOM(pixel.canvas, 'game');
         
            //  Cache the width/height to avoid looking it up every render
            //pixel.width = pixel.canvas.width;
            //pixel.height = pixel.canvas.height;
            pixel.width = game.width * pixel.scale;
            pixel.height = game.height * pixel.scale;

            // Just to make sure it's the right scale.
            pixel.canvas.width = pixel.width;
            pixel.canvas.height = pixel.height;

            // ...
            game.renderer.renderSession.roundPixels = true;
            game.stage.smoothed = false;

            this.physics.startSystem(Phaser.Physics.ARCADE);

        },

        render: function () {

            // Every loop we need to render the un-scaled game canvas to the displayed scaled canvas:
            pixel.context.drawImage(game.canvas, 0, 0, game.width, game.height, 0, 0, pixel.width, pixel.height);
         
        },

        preload: function () {

            // game.scale.pageAlignHorizontally = true;
            // game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

            //  We need this because the assets are on Amazon S3
            //  Remove the next 2 lines if running locally
            // this.load.baseURL = 'http://files.phaser.io.s3.amazonaws.com/codingtips/issue007/';
            // this.load.crossOrigin = 'anonymous';

            //  Disable smoothing on the scaled canvas
            Phaser.Canvas.setSmoothingEnabled(pixel.context, false);

            this.load.baseURL = 'resources/';

            this.load.image('background', 'assets/background.gif');
            this.load.image('background-forward', 'assets/background-forward.gif');
            this.load.image('foreground', 'assets/foreground.png');
            
            this.load.image('player', 'assets/ship.png');
            this.load.image('enemyalpha', 'assets/enemyalpha.png');
            this.load.image('turretalpha', 'assets/turretalpha.png');

            this.load.bitmapFont('shmupfont', 'assets/shmupfont.png', 'assets/shmupfont.xml');

            this.load.spritesheet('thrustalpha', 'assets/thrustalpha.png', 13, 6, 4);
            this.load.spritesheet('explosion', 'assets/exp2_0.png', 30, 30);

            for (var i = 1; i <= 11; i++)
            {
                this.load.image('bullet' + i, 'assets/bullet' + i + '.png');
            }

            // Weapon power ups
            this.load.image('gatlingPower', 'assets/wep1.gif');
            this.load.image('beamPower', 'assets/wep2.gif');

            // Standard pickups
            this.load.image('shields', 'assets/shields.gif');
            this.load.image('specials', 'assets/specials.gif');
            this.load.image('ovShields', 'assets/ovShields.gif');

            //  Note: Graphics are not for use in any commercial project (the ones that came with phaser example, weapons, etc)

        },

        create: function () {

            this.background = this.add.tileSprite(0, 0, this.game.width, this.game.height, 'background');
            this.backgroundfwd = this.add.tileSprite(0, 0, this.game.width, this.game.height, 'background-forward');
            // this.foreground = this.add.tileSprite(0, 0, this.game.width * 2, this.game.world.height, 'foreground');

            // If in space, would the BG actually move? Maybe we shouldn't go in to space...
            // https://www.youtube.com/watch?v=Y4P8iqyMnS0 <-- Example of how else to do it.
            this.background.autoScroll(0, 0);
            this.backgroundfwd.autoScroll(-30, 0);
            // this.foreground.autoScroll(-10, 0);

            /// Add singulars
            
            // Bullet type one
            this.weaponsCache.push(new WeaponRegistry.SingleBullet(this.game).setType(weapons.SINGLEBULLET).setUpgrade(weapons.FRONTANDBACK));
            this.weaponsCache.push(new WeaponRegistry.FrontAndBack(this.game).setType(weapons.FRONTANDBACK));

            // Bullet type two
            this.weaponsCache.push(new WeaponRegistry.Beam(this.game).setType(weapons.BEAM));
            
            // this.weaponsCache.push(new WeaponRegistry.ThreeWay(this.game));
            // this.weaponsCache.push(new WeaponRegistry.EightWay(this.game));
            // this.weaponsCache.push(new WeaponRegistry.ScatterShot(this.game));
            // this.weaponsCache.push(new WeaponRegistry.SplitShot(this.game));
            // this.weaponsCache.push(new WeaponRegistry.Pattern(this.game));
            // this.weaponsCache.push(new WeaponRegistry.Rockets(this.game));
            // this.weaponsCache.push(new WeaponRegistry.ScaleBullet(this.game));

            // Add combinations
            // Currently broken, these need to work like other weapons in terms of resetting, etc. - TODO.
            // this.weaponsCache.push( new WeaponRegistry.WeaponCombo().combine("Combo A", WeaponRegistry.SingleBullet, WeaponRegistry.Rockets) );
            // this.weaponsCache.push( new WeaponRegistry.WeaponCombo().combine("Combo B", WeaponRegistry.ThreeWay, WeaponRegistry.Pattern, WeaponRegistry.Rockets) );
            this.game.foreground = this.add.sprite(0, 0, 'foreground');

            this.game.foreground.addChild(this.add.sprite(172, 130, 'turretalpha'));
            this.game.foreground.addChild(this.add.sprite(216, 130, 'turretalpha'));
            this.game.foreground.addChild(this.add.sprite(284, 174, 'turretalpha'));

            this.thruster = this.add.sprite(-5, 10, 'thrustalpha');
            this.thruster.animations.add('default');
            this.thruster.animations.play('default', 24, true);
            this.thruster.anchor.set(0.5);

            this.player = this.add.sprite(48, ((this.game.height - 12) / 2), 'player');
            this.player.addChild(this.thruster);
            this.player.stats = {
                shields: 100,
                specials: 3
            }

            this.physics.arcade.enable(this.player);

            this.player.body.collideWorldBounds = true;

            this.weaponsCache[this.currentWeapon].x += this.player.width / 2;

            //  Cursor keys to fly + space to fire
            this.cursors = this.input.keyboard.createCursorKeys();

            this.input.keyboard.addKeyCapture([ Phaser.Keyboard.SPACEBAR ]);

            // var changeKey = this.input.keyboard.addKey(Phaser.Keyboard.ENTER);
            // changeKey.onDown.add(this.nextWeapon, this);

            // Initialize UI
            // You can enter multiple properties here and bind them.
            gameUI.populate({
                playerName: "Skurrai"
            });

            // Or, you can do them individually. This way is the best to use after initialization.
            gameUI.set("currentWeapon", this.weaponsCache[this.currentWeapon].getName());
            gameUI.set("specials", this.player.stats.specials);
            gameUI.set("shields", this.player.stats.shields);

            // Level-based
            // This is just pseudo code to give an idea.
            var uiHeight = 84;
            game.world.setBounds(0, 0, game.width, game.height);
            game.stage.smoothed = false;

            // Pickups like enemies and bullets belong to a group
            this.game.pickups = game.add.group();
            this.game.pickups.enableBody = true;
            this.game.pickups.physicsBodyType = Phaser.Physics.ARCADE;

            // Create a pickup to start with - Need to document how these weapons work...
            // Or better yet, just pass the same data for each, and if there isn't one in the chain, it'll just bind it self?
            // name, opts, element type, initialBindingIndex, x, y
            // var pickup = pickupFactory.create('gatlingPower', pickups.POWER, pickups.POWER.elementTypes.gatling, weapons.SINGLEBULLET.type, game.width / 2, 20);
            // this.game.pickups.add(pickup);

            // pickup = pickupFactory.create('beamPower', pickups.POWER, null, weapons.BEAM.type, game.width / 2, 40);
            // this.game.pickups.add(pickup);

            // pickup = pickupFactory.create('shields', pickups.SHIELDS, null, null, game.width / 2, 60);
            // this.game.pickups.add(pickup);

            // pickup = pickupFactory.create('ovShields', pickups.SHIELDSOV, null, null, game.width / 2, 80);
            // this.game.pickups.add(pickup);

            // pickup = pickupFactory.create('specials', pickups.SPECIAL, null, null, game.width / 2, 100);
            // this.game.pickups.add(pickup);

            // All enemies belong to the games enemy group
            this.game.enemies = game.add.group();
            this.game.enemies.enableBody = true;
            this.game.enemies.physicsBodyType = Phaser.Physics.ARCADE;

            // Create an enemy to start with
            // Needs a 'position' factor.
            // File name, sprite name, number to make
            // this.enemy = creatureFactory.create('TestEnemy', 'enemyalpha', 1);
            // this.enemy = creatureFactory.create('TestEnemyWithPath', 'enemyalpha', 1, {
            //     pathType: "RightToLeft", // 0
            //     speedAlongPath: 2
            // });
            // this.game.enemies.add(this.enemy);

            // Finalize weapon positions and set future targets
            this.currentWeapon = 0;
            for (var i = 1; i < this.weaponsCache.length; i++)
            {
                this.weaponsCache[i].visible = false;
                this.weaponsCache[i].x += this.player.width / 2;
            }

            // Map creation test
            mapFactory.runTest();

            // Cluster (line) test
            // this.cluster = creatureFactory.create('ClusterAlpha', 'enemyalpha', 1, {
            //     pathType: "TestPath", // 0
            //     speedAlongPath: 2
            // });
            // this.game.enemies.add(this.cluster);

            // this.cluster2 = creatureFactory.create('ClusterAlpha', 'enemyalpha', 1, {
            //     pathType: "TestPathReversed", // 0
            //     speedAlongPath: 2
            // });
            // this.game.enemies.add(this.cluster2);

            // Cluster (formation / flock) test
            // ...

            // Cam follow
            // this.camera.follow(this.player, Phaser.Camera.FOLLOW_LOCKON);
            // this.player.fixedToCamera = true;

        },

        cleanWeapon: function () {

            if(!this.weaponsCache[this.currentWeapon]) {
                console.error("Error getting weapon, you may have forgotten to set an element type somewhere.");
                return;
            }

            //  Tidy-up the current weapon
            this.weaponsCache[this.currentWeapon].visible = false;
            this.weaponsCache[this.currentWeapon].callAll('reset', null, 0, 0);
            this.weaponsCache[this.currentWeapon].setAll('exists', false);
            this.weaponsCache[this.currentWeapon].visible = true;
            
            gameUI.set("currentWeapon", this.weaponsCache[this.currentWeapon].getName());

        },

        update: function () {

            this.game.foreground.x -= 0.3;

            this.player.body.velocity.set(0);

            if (this.cursors.left.isDown)
            {
                this.player.body.velocity.x = -this.speed;
            }
            else if (this.cursors.right.isDown)
            {
                this.player.body.velocity.x = this.speed;
            }

            if (this.cursors.up.isDown)
            {
                this.player.body.velocity.y = -this.speed;
            }
            else if (this.cursors.down.isDown)
            {
                this.player.body.velocity.y = this.speed;
            }

            if (this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR))
            {
                this.weaponsCache[this.currentWeapon].fire(this.player);
            }

            // As we don't need to exchange any velocities or motion we can the 'overlap' check instead of 'collide'
            this.physics.arcade.overlap(this.weaponsCache[this.currentWeapon].children, this.game.enemies, this.onPlayerBulletHitsEnemy, null, this);
            this.physics.arcade.overlap(this.player, this.game.enemies, this.onEnemyBumpsPlayer, null, this);
            this.physics.arcade.overlap(this.player, this.game.pickups, this.onPlayerPickup, null, this);

            // If an enemy was just killed, make another. Just for testing.
            this.game.enemies.children.forEach(function(child){

                // Not quite right, ensure it's done properly using damage call, etc.
                // http://phaser.io/docs/2.4.4/Phaser.Sprite.html
                // http://phaser.io/docs/2.4.4/Phaser.Group.html#getFirstDead
                if(!child.alive && child.entityProperties.pathType)
                    this.game.enemies.getFirstDead(false).respawn();

            }, this);

        },

        onPlayerPickup: function(player, pickup) {

            if(pickup.type === pickups.POWER.type) {

                if(pickup.elementType === this.weaponsCache[this.currentWeapon].elementType) {
                    
                    // The new way we're dealing with weapon upgrades might just do away
                    // with power altogether. Food for thought, it might need a UI revisit but
                    // keep it for now. Could potentially use it to show the weapons upgrade
                    // level instead which would be pretty cool.

                    // To prevent further upgrades, just don't set the last weapon in the chains upgrade type.
                    if(this.weaponsCache[this.currentWeapon].upgradeType > -1) {
                        this.currentWeapon = this.weaponsCache[this.currentWeapon].upgradeType;
                        gameUI.set('power', gameUI.get('power') + pickup.value);
                    }

                    // You might still want this for various specials, etc.
                    // this.weaponsCache[this.currentWeapon].increaseBulletDamage(pickup.value);
                    // If you have the above, you'll also probably want this.
                    // this.weaponsCache[this.currentWeapon].resetBulletDamage();

                } else {
                    
                    gameUI.set('power', 0);

                    /*
                    Issue here is that element type doesn't match that of the weapons index in the weapon cache array
                    that we're after.
                    So what we really need to do is find the first instance, or, lowest powered version of said weapon
                    when we're changing weapons again.
                    */
                    // So it's a whole new weapon. Pass the type properly
                    this.currentWeapon = pickup.initialBindingIndex;
                
                }

                this.cleanWeapon();

                ///////////////////// tests
                // Make another
                // var pickupNum = game.rnd.integerInRange(0, 1),
                // pickup, pickupb;

                // // Create another gatling type pickup to test upgrade
                // // It seems that pickups might actually need a property that binds them to an 'initial' state. So for
                // // weapons you need to have an initial binding that it uses. This will allow to do an initial call,
                // // then subsequent ones after that. Good for weapons, wont break other stuff.
                // pickup = pickupFactory.create('gatlingPower', pickups.POWER, pickups.POWER.elementTypes.gatling, weapons.SINGLEBULLET.type);

                // pickup.x = game.rnd.integerInRange(game.width - 40, game.width - 100);
                // pickup.y = game.rnd.integerInRange(50, game.world.height - 50);
                
                // this.game.pickups.add(pickup);

                // // Create a beam pickup type, this shouldn't affect anything as there's no upgrade type set
                // pickupb = pickupFactory.create('beamPower', pickups.POWER, null, weapons.BEAM.type);

                // pickupb.x = game.rnd.integerInRange(game.width - 40, game.width - 100);
                // pickupb.y = game.rnd.integerInRange(50, game.world.height - 50);
                
                // this.game.pickups.add(pickupb);
            
            }

            if(pickup.type === pickups.SHIELDS.type && this.player.stats.shields < 100) {

                var shieldVal = pickups.SHIELDS.value;

                if(this.player.stats.shields + shieldVal > 100) {
                   
                    gameUI.set('shields', 100);
                    this.player.stats.shields = 100;

                } else {

                    gameUI.set('shields', this.player.stats.shields + shieldVal);
                    this.player.stats.shields += shieldVal;

                }

            }

            if(pickup.type === pickups.SHIELDSOV.type) {

                var ovShieldVal = pickups.SHIELDSOV.value;

                if(this.player.stats.shields + ovShieldVal > 255) {
                   
                    gameUI.set('shields', 255);
                    this.player.stats.shields = 255;

                } else {

                    gameUI.set('shields', this.player.stats.shields + ovShieldVal);
                    this.player.stats.shields += ovShieldVal;

                }

                gameUI.set('shields', 255);
                this.player.stats.shields = 255;

            }

            if(pickup.type === pickups.SPECIAL.type) {

                gameUI.set('specials', this.player.stats.specials + pickups.SPECIAL.value);
                this.player.stats.specials = this.player.stats.specials + pickups.SPECIAL.value;

            }

            // Destroys, may want to just kill and get first of type when you need another.
            pickup.doDeath(true);

        },

        onPlayerBulletHitsEnemy: function(projectile, target) {
            
            // Apply the base damage to the target (this will be further assessed by the entity)
            target.takeDamage(projectile.getDamage());

            // Kill will not completely remove from memory. This avoids re-creation.
            projectile.doDeath();

            // Update score based off value of enemy or bullet (not yet implemented)
            gameUI.set('score', gameUI.get('score') + 10);

            if(!target.alive)
                gameUI.set('score', gameUI.get('score') + 10000);

        },

        onEnemyBumpsPlayer: function(player, enemy) {

            if(godmode)
                return;

            var baseDamage = 1;

            // It'd be better to base the damage from the enemies mass or something, possibly their speed too.
            if(player.stats.shields - baseDamage <= 0) {
                player.stats.shields = 0;
                player.kill();
            } else {
                player.stats.shields -= baseDamage;
            }

            gameUI.set('shields', player.stats.shields);

        }

    };

    game.state.add('Game', PhaserGame, true);

});	