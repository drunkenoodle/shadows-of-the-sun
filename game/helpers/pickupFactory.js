define(['game', 'sprites/Pickup'], function(game, Pickup){

	var PickupFactory = function(){};

	PickupFactory.prototype.create = function(name, opts, eType, initialBindingIndex, x, y)
	{

		return this.createSinglePickup(name, opts, eType, initialBindingIndex, x, y);

	}

	PickupFactory.prototype.createSinglePickup = function(name, opts, eType, initialBindingIndex, x, y)
	{

        var pickup = new Pickup(game, x, y, name);
        game.physics.arcade.enable(pickup);

        pickup.value = opts.value;
        pickup.type = opts.type;

        if(!isNaN(eType))
	        pickup.elementType = eType;

	    if(!isNaN(initialBindingIndex))
	        pickup.initialBindingIndex = initialBindingIndex;

        return pickup;

	}

	return new PickupFactory;

});