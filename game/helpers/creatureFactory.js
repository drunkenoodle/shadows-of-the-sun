define([
	'game',
	'sprites/TestEnemy',
	'sprites/TestEnemyWithPath',
	'sprites/TurretAlpha',
	'sprites/ClusterAlpha'
], function(game, TestEnemy, TestEnemyWithPath, TurretAlpha, ClusterAlpha){

	var CreatureFactory = function(){};

	CreatureFactory.prototype.creatureTypes = {
		TestEnemy: TestEnemy,
		TestEnemyWithPath: TestEnemyWithPath,
		TurretAlpha: TurretAlpha,
		ClusterAlpha: ClusterAlpha
	}

	CreatureFactory.prototype.create = function(fileName, name, num, entityProperties)
	{

		// Better to use dot notation if possible.
		var cType = this.creatureTypes[fileName];

		if(num > 1) {
			// ... You'd generate this the same way you do bullets.
		} else {
			// Same as above, but we're just testing for now.
			return this.createSingleEnemy(cType, name, entityProperties);
		}

	}

	CreatureFactory.prototype.createSingleEnemy = function(Constructor, name, entityProperties)
	{
		
		if(!entityProperties)
			entityProperties = {
				x: 0,
				y: 0
			}

        this.entity = new Constructor(game, entityProperties.x, entityProperties.y, name, entityProperties);
        game.physics.arcade.enable(this.entity);

        return this.entity;

	}

	return new CreatureFactory;

});