define(['game', 'creatureFactory'], function(game, creatureFactory){

	var MapFactory = function(){};

	// Use this in future
	const behaviourTypes = {
		TRACKPLAYER: 0
	}

	// TODO: Make sprite names same as constructor names so it's less bulk.
	// Should have a global place for all these, will do this soon.
	const entityTypes = {
		TURRETALPHA: {
			type: 0,
			constructorName: 'TurretAlpha',
			spriteName: 'turretalpha',
			defaultBehaviour: behaviourTypes.TRACKPLAYER
		}
	}

	// Data example for map
	var mapData = {

		entityPlacement: [
			{
				constructorName: entityTypes.TURRETALPHA.constructorName,
				spriteName: entityTypes.TURRETALPHA.spriteName,
				type: entityTypes.TURRETALPHA.type,
				x: 384,
				y: 174
			},
			{
				constructorName: entityTypes.TURRETALPHA.constructorName,
				spriteName: entityTypes.TURRETALPHA.spriteName,
				type: entityTypes.TURRETALPHA.type,
				x: 768,
				y: 174
			},
			{
				constructorName: entityTypes.TURRETALPHA.constructorName,
				spriteName: entityTypes.TURRETALPHA.spriteName,
				type: entityTypes.TURRETALPHA.type,
				x: 1152,
				y: 174
			}
		]

	}

	MapFactory.prototype.runTest = function()
	{

		// Spawn entities / placeholders - Todo: placeholders would reduce memory, so we only call spawn when they're near to appearing on screen.
		mapData.entityPlacement.forEach(function(ent){

			var enemy = creatureFactory.create(ent.constructorName, ent.spriteName, 1);
			enemy.staticMove(ent.x, ent.y);

            game.enemies.add(enemy);

		});

	}

	return new MapFactory;

});